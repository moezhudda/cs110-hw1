package edu.ucsd.cse110.library;

public class Member {
	private MemberType memberType;
	private String name;
	private double fees; 
	public Member(String string, MemberType type) {
		setName(string);
		memberType = type;
	}

	public double getDueFees() {
		return fees;
	}

	public void applyLateFee(double i) {
		fees+=i;		
	}
	
	public MemberType getType() {
		return memberType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
