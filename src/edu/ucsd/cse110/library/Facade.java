package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class Facade {
	public void checkoutPublication(Member a, Publication b)
	{
		LocalDate dateCheckedOut = LocalDate.now();
		b.checkout(a, dateCheckedOut);
	}
	
	public void checkoutPublication(Member a, Publication b, LocalDate c)
	{
		b.checkout(a,c);
	}
	
	public void returnPublication(Publication a)
	{
		LocalDate dateReturned = LocalDate.now();
		a.pubReturn(dateReturned);
	}
	
	public void returnPublication(Publication a, LocalDate b)
	{
		a.pubReturn(b);
	}
	
	public double getFee(Member a)
	{
		return a.getDueFees();
	}
	
	public boolean hasFee(Member a)
	{
		if (a.getDueFees() != 0)
			return true;
		else
			return false;
	}
}
