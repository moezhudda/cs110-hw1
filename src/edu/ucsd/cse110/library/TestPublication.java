package edu.ucsd.cse110.library;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.ucsd.cse110.library.rules.RuleObjectBasedLateFeesStrategy;

public class TestPublication {
	
	Publication pub;
	Member max;
	Facade pattern;
	
	@Before
	public void setUp() {
		pub = new Book("Lords of the Rings", new RuleObjectBasedLateFeesStrategy());
		max = new Member("Max", MemberType.Teacher);
		
		pattern = new Facade();
	}
	
	@Test
	public void testCheckout() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		pattern.checkoutPublication(max,pub,checkoutDate);
		assertTrue(pub.isCheckout());
		assertEquals(max, pub.getMember());
		assertEquals(checkoutDate, pub.getCheckoutDate());
	}
	
	@Test
	public void testReturnOk() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 3);
		pattern.checkoutPublication(max, pub, checkoutDate);
		pattern.returnPublication(pub, returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(0.00, pattern.getFee(max),0.01);
	}
	
	@Test
	public void testReturnLate() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		pattern.checkoutPublication(max, pub, checkoutDate);
		pattern.returnPublication(pub, returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(5.00, pattern.getFee(max),0.01);
	}
	
}
